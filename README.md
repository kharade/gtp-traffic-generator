# GTP Traffic Generator

GTP traffic generator for benchmarking 5G user plane

* Build gtp traffic generator 
```bash
$ cd gtp-traffic-generator/
$ make
```

* Usage
```bash
$ ./trfgen -h
GTP Packet Generator
Usage: ./trfgen [OPTIONS]

Options:
  -h,--help                   Print this help message and exit
  --teid UINT                 TEID for PFCP session at UPF
  --threads INT               Number of threads
  --packets INT               Number of packets per thread
  --packet_size INT           Packet size in Bytes
  --ue_src TEXT               Ip address of UE
  --ue_dst TEXT               Ip address of DN
  --gtpu_peer TEXT            Ip address of N3 on UPF
```
* Sample Usage
```bash
$ ./trfgen --teid 0x000004d2 --ue_src 12.1.1.2 --ue_dst 192.168.20.3 --gtpu_peer 192.168.18.210 --threads 3 --packets 1000 --packet_size 1024
```
* Sample Output
```bash
Creating 3 number of threads 

 Sending 1000 packets of length 4096 Bytes 
 Sending 1000 packets of length 4096 Bytes 
 Sending 1000 packets of length 4096 Bytes 
 Time duration:- 0.396436 Sec......... Tx Data Rate :- 82.656431 Mbps
 Time duration:- 0.396454 Sec......... Tx Data Rate :- 82.652693 Mbps
 Time duration:- 0.400706 Sec......... Tx Data Rate :- 81.775620 Mbps
```
