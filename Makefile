XX := g++
CXXFLAGS += -I. -std=c++11
LDFLAGS  +=

TARGET = trfgen
SRCS = trfgen.c
OBJS = $(SRCS:.cc=.o)

.cc.o:
	@echo " CXX $@"
	@$(CXX) $(CXXFLAGS) -c $< -o $@


$(TARGET): $(OBJS)
	@echo " Bulding done $@"
	@$(CXX) $(CXXFLAGS) $(OBJS) -o $(TARGET) -lpthread $(LDFLAGS)

clean:
	rm -f $(TARGET) *.o *.out

run:
	@./trfgen

install:
	sudo cp $(TARGET) /usr/local/bin

uninstall:
	sudo rm -f /usr/local/bin/$(TARGET)
