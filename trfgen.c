#include "trfgen.h"

using namespace std;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

struct args {
  struct sockaddr_in servaddr;
  struct final_packet gtp_packet;
  int gtp_len_tot, sockfd;
};

void create_gtp_packet(int sockfd, struct sockaddr_in servaddr,
                       struct args *arg) {
  int header_length, gtp_len_tot;
  char payload_data[] =
      "OpenairInterface Software Alliance - VPP Benchmarking Test";

  // Creating GTP Packet
  header_length = sizeof(struct gtp_ext) + sizeof(struct gtp) +
                  sizeof(struct ip) + sizeof(struct udp);

  struct gtp_ext gtp_exth;
  memset(&gtp_exth, 0, sizeof(struct gtp_ext));
  gtp_exth.len           = 0x01;
  gtp_exth.pdu_type      = 0x10;
  gtp_exth.qfi           = 0x05;
  gtp_exth.next_ext_type = 0x00;

  struct gtp gtph;
  memset(&gtph, 0, sizeof(struct gtp));
  gtph.flags             = 0x34;
  gtph.msg               = 0xff;
  gtph.len               = htons(GTP_EXT_HDR_OFFSET + sizeof(struct gtp_ext) +
                   sizeof(struct ip) + sizeof(struct udp));
  gtph.teid              = htonl(TEID);
  gtph.pdu_number        = 0x00;
  gtph.sequence          = 0x00;
  gtph.next_ext_type     = 0x85;

  struct ip iph;
  memset(&iph, 0, sizeof(struct ip));
  iph.ver_hlen           = 0x45;
  iph.service            = 0x00;
  iph.length             = htons(sizeof(struct ip) + sizeof(struct udp));
  iph.ident              = 0x01;
  iph.fragment           = 0x00;
  iph.timetolive         = 0x40;
  iph.protocol           = 0x11;
  iph.checksum           = 0x1b98;
  iph.src_addr           = inet_addr(UE_SRC.c_str());
  iph.dest_addr          = inet_addr(UE_DST.c_str());

  struct udp udph;
  memset(&udph, 0, sizeof(struct udp));
  udph.src_port          = htons(80);
  udph.dst_port          = htons(80);
  udph.hdr_len           = htons(sizeof(struct udp));
  udph.checksum          = 0xc369;
  strcpy(udph.data, payload_data);
  gtp_len_tot = header_length;

  // struct args arg;
  arg->gtp_len_tot       = gtp_len_tot;
  arg->servaddr          = servaddr;
  arg->sockfd            = sockfd;
  arg->gtp_packet.p1     = gtph;
  arg->gtp_packet.p2     = gtp_exth;
  arg->gtp_packet.p3     = iph;
  arg->gtp_packet.p4     = udph;
}

void *send_gtp(void *input) {
  double packet_counter, packets, total_bits, total_time;
  int gtp_len_tot, sockfd;
  struct sockaddr_in servaddr;
  packet_counter         = NUMBER_OF_PACKETS;

  gtp_len_tot            = ((struct args *)input)->gtp_len_tot;
  sockfd                 = ((struct args *)input)->sockfd;
  servaddr               = ((struct args *)input)->servaddr;

  printf("\n Sending %ld packets of length %d Bytes \n",
         long(NUMBER_OF_PACKETS), gtp_len_tot);

  auto begin             = std::chrono::high_resolution_clock::now();
  while (packet_counter > 0) {
    sendto(sockfd, (void *)&((struct args *)input)->gtp_packet, gtp_len_tot, 0,
           (const struct sockaddr *)&servaddr, sizeof(servaddr));
    // NSEC_SLEEP(1);
    usleep(100);
    packet_counter--;
  }
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::nano> time_spent = (end - begin);

  total_time             = (time_spent.count() * 1e-9);
  total_bits             = NUMBER_OF_PACKETS * double(gtp_len_tot) * 8;

  printf("\n Time duration:- %lf Sec......... Tx Data Rate :- %lf Mbps\n",
         total_time, double((total_bits * 1e-6) / total_time));

  return 0;
}

int main(int argc, char *argv[]) {
  int sockfd, thread_create, thread_join, opt;
  struct sockaddr_in servaddr;

  // Parse CLI args
  CLI::App app{"GTP Packet Generator"};
  app.add_option("--teid", TEID, "TEID for PFCP session at UPF");
  app.add_option("--threads", NUMBER_OF_THREADS, "Number of threads");
  app.add_option("--packets", NUMBER_OF_PACKETS,
                 "Number of packets per thread");
  app.add_option("--packet_size", packet_size, "Packet size in Bytes");
  app.add_option("--ue_src", UE_SRC, "Ip address of UE");
  app.add_option("--ue_dst", UE_DST, "Ip address of DN");
  app.add_option("--gtpu_peer", GTPU_PEER, "Ip address of N3 on UPF");
  CLI11_PARSE(app, argc, argv);
  thread_create = thread_join = NUMBER_OF_THREADS;

  // Creating UDP socket on N3 Interface
  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family      = AF_INET;
  servaddr.sin_port        = htons(GTPU_PORT);
  servaddr.sin_addr.s_addr = inet_addr(GTPU_PEER.c_str());

  // Initialize arguments for threads
  struct args arg; // = NULL;
  create_gtp_packet(sockfd, servaddr, &arg);

  // Create threads
  printf("Creating %d number of threads \n", NUMBER_OF_THREADS);
  pthread_t tids[NUMBER_OF_THREADS];
  for (int i = 0; thread_create > 0; i++) {
    pthread_create(&tids[i], NULL, send_gtp, (void *)&arg);
    thread_create--;
  }

  // Join Threads
  for (int i = 0; thread_join > 0; i++) {
    pthread_join(tids[i], NULL);
    thread_join--;
  }

  close(sockfd);
  return 0;
}